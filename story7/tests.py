from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import accordion
from .apps import Story7Config

class Story7UnitTest(TestCase) :
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('')
        self.page_content = self.response.content.decode('utf8')

    def test_story7_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story7_check_template_used(self):
        self.assertTemplateUsed(self.response, 'accordion.html')

    def test_story7_content_exists(self):
        self.assertIn('MY ACTIVITY ', self.page_content)
        self.assertIn('MY EXPERIENCE', self.page_content)
        self.assertIn('MY HOBBY', self.page_content)
        self.assertIn('MY FAVORITE MOVIE', self.page_content)
