from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import searchbook, data
from .apps import Story8Config
import unittest
# Create your tests here.
class UnitTest(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/books/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_data_json_tersedia(self):
        response = Client().get("/books/data/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/books/")
        self.assertEqual(found.func, searchbook)
    
    def test_apakah_mengakses_function_page_json_yang_benar(self):
        found = resolve("/books/data/")
        self.assertEqual(found.func, data)

    def test_apakah_template_terpakai(self):
        response = Client().get("/books/")
        self.assertTemplateUsed(response, 'searchbook.html')

