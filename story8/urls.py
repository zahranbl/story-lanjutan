from django.contrib import admin
from django.urls import path
from .views import searchbook, data

app_name = 'story8'

urlpatterns = [
    path('', searchbook, name='searchbook'),
    path('data/', data, name='data'),
]
