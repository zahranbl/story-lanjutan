from django.urls import path
from django.contrib import admin
from . import views

app_name = 'login'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
]