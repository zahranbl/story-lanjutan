from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .forms import * 
from .apps import AppConfig
from django.http import HttpRequest
from .urls import urlpatterns
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, get_user


# Create your tests here.
class AuthTest(TestCase):

    # TODO: Test for django messages, test login errors

    def test_register_password_didnt_match(self):
        register_data = {
            "username": "test_dummy_different_pass",
            "password1": "abc5dasar",
            "password2": "abc5dodol"
        }
        
        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "two password fields didn’t match")

    def test_register_username_taken(self):
        register_data = {
            "username": "test_dummy_duplicate",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 302)
        
        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "user with that username already exists")

    def test_register_password_too_common(self):
        register_data = {
            "username": "test_dummy_too_common",
            "password1": "asdf12345",
            "password2": "asdf12345"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "password is too common")
        
    def test_success_register_login_logout(self):
        register_data = {
            "username": "test_dummy",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/login/')

        # Login user
        login_data = {
            "username": "test_dummy",
            "password": "abc5dasar"
        }
        response = self.client.post('/login/', data=login_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/home/')
        
        # Check auth status after login
        user = get_user(self.client)
        self.assertTrue(user.is_authenticated)

        # Logout user
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/home/')
        
        # Check auth status after logout
        user = get_user(self.client)
        self.assertFalse(user.is_authenticated)

